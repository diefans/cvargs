import pytest


def test_convert_args():
    import cvargs

    def foo(value):
        return divmod(value, 2)

    @cvargs.convert(a=str, b=float, c=lambda x: x * 2, d=lambda x: not x, e=foo, f=bool)
    def func(a, b=None, *c, d, e=None, **f):
        return a, b, c, d, e, f

    result = func(123, "1.23", 1, 2, d=True, e=123, c=0)

    assert result == ("123", 1.23, (2, 4), False, (61, 1), {"c": False})


@pytest.mark.parametrize("args, result", [((1, "1", 0, 1), ("1", 1.0, (False, True)))])
def test_convert_varargs(args, result):
    import cvargs

    @cvargs.convert(a=str, b=float, c=bool)
    def func(a, b, *c):
        return a, b, c

    assert func(*args) == result


@pytest.mark.parametrize("args, result", [((1, "1", 0, 1), ("1", 1.0, (0, 1)))])
def test_convert_no_varargs(args, result):
    import cvargs

    @cvargs.convert(a=str, b=float)
    def func(a, b, *c):
        return a, b, c

    assert func(*args) == result


def test_convert_varkw():
    import cvargs

    @cvargs.convert(a=str, d=float)
    def func(a, *b, c, **d):
        return a, b, c, d

    assert func(1, 1.2, b="1.2", c=False, d="1.2") == (
        "1",
        (1.2,),
        False,
        {"b": 1.2, "d": 1.2},
    )


def test_convert_varkw_no_converter():
    import cvargs

    @cvargs.convert(a=str)
    def func(a, *b, c, **d):
        return a, b, c, d

    assert func(1, 1.2, b="1.2", c=False, d="1.2") == (
        "1",
        (1.2,),
        False,
        {"b": "1.2", "d": "1.2"},
    )
